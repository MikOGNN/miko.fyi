const shorturls = require("../public/urls.json");
const fs        = require("fs");
const path      = require("path");

const outpath = path.join(__dirname, "..", "public");
let   output  = "";

if (!fs.existsSync(outpath)) {
	console.error(`ERROR: could not write '_redirects': '${outpath}' does not exist.`);
	process.exit(1);
}

for (const url of shorturls)
	output += `/${url.slug} ${url.url} 301\n`;

fs.writeFileSync(path.join(outpath, "_redirects"), output);