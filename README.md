# gitly
– the static url-shortener, hosted on GitLab Pages

<div align="center" markdown>

![](assets/logo_gitly_full.svg){width=250px}

</div>
