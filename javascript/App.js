import I18n     from "./I18n.js";
import LinkList from "./LinkList.js";
class App {
	linkList;
	i18n;

	constructor() {
		this.linkList = new LinkList();
		this.i18n     = new I18n();
	}
}

export default App;